#include <iostream>
#include <curl/curl.h>

// GitLab API endpoint and personal access token

#define API_ENDPOINT "https://gitlab.com/manoz1234/review-dashboard"
#define ACCESS_TOKEN "glpat-GVSQzhSYevDPjv-JaBK1"

// GitLab project ID and URL-encoded query string for searching comments
#define PROJECT_ID "45260506"
#define QUERY_STRING "search=review dashboard"

int main() {
   // Initialize libcurl and set options
  curl_global_init(CURL_GLOBAL_ALL);
  CURL *curl = curl_easy_init();
  if (curl) {
   // Disable verification of peer certificate
   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

   // Set headers
   struct curl_slist *headers = NULL;
   headers = curl_slist_append(headers, "Content-Type: application/json");
   headers = curl_slist_append(headers, "Authorization: Bearer " ACCESS_TOKEN);
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

   // Print project name
   std::string project_name_url_encoded;
   project_name_url_encoded.reserve(256);
   curl_easy_setopt(curl, CURLOPT_URL, API_ENDPOINT "/projects/" PROJECT_ID);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &project_name_url_encoded);
   curl_easy_perform(curl);
   curl_slist_free_all(headers);

   size_t name_begin = project_name_url_encoded.find("\"name\":\"");
   size_t name_end = project_name_url_encoded.find("\",\"name_with_namespace\"");
   if (name_begin != std::string::npos && name_end != std::string::npos) {
   std::string project_name = project_name_url_encoded.substr(name_begin + 8, name_end - name_begin - 8);
   std::cout << "Project name: " << project_name << std::endl;
   } else {
     std::cout << "Failed to retrieve project name" << std::endl;
   }

    // Search for comments
   std::string comments_url = API_ENDPOINT "/projects/" PROJECT_ID "/merge_requests/" QUERY_STRING;
   curl_easy_setopt(curl, CURLOPT_URL, comments_url.c_str());

   std::string comments_response;
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &comments_response);
   CURLcode res = curl_easy_perform(curl);

   if (res != CURLE_OK) {
   std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
   } else {
   // Extract comment body
   size_t body_begin = comments_response.find("\"body\":\"");
   size_t body_end = comments_response.find("\",\"created_at\"");
   if (body_begin != std::string::npos && body_end != std::string::npos) {
    std::string comment_body = comments_response.substr(body_begin + 8, body_end - body_begin - 8);
    std::cout << "Comment body: " << comment_body << std::endl;
    } else {
     std::cout << "Failed to retrieve comment body" << std::endl;
    }
    }

     // Cleanup
     curl_easy_cleanup(curl);
    }

    curl_global_cleanup();

    return 0;
}
